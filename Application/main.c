#include "stm32f4xx.h"

uint16_t delay_c = 0;


void SysTick_Handler(void){
if(delay_c > 0)
delay_c--;
}

void delay_ms(uint16_t delay_t){
delay_c = delay_t;
while(delay_c){}
}

void setup(){
		
	  SysTick_Config(SystemCoreClock/1000); //setHandler

		RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN; // enable clocking for port D

		GPIOD->MODER = 0x55000000; // 01 01 | 00 00 | 00 00 | 00 00

		GPIOD->OTYPER = 0; // 0 = output

		GPIOD->OSPEEDR = 0; //lowest speed
}

int main(){
	
		setup();
	
		while(1){

			GPIOD->ODR = 0x9000;

			delay_ms(500);

			GPIOD->ODR = 0x0000;

			delay_ms(500);

		}
}